default:
	make server

dev:
	make client-once && make server

server:
	@PORT=4444 coffee push-server.coffee

client-once:
	coffee -c public/client.coffee

client:
	coffee -cw public/client.coffee
