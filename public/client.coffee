App = Ember.Application.create()

App.Options =
  sample_count: 10
  sampling_interval_ms: 500
  broken_sensor_reading: 4095
  max_sensor_reading: 4095

App.Router.map ->
  @resource 'sensors', ->
    @route 'sensor', {path: '/:sensor_no'}

window.sensors = sensors = Ember.A([
  Ember.Object.create
    id: 1
    desc: "Prechamber, ground"
    points: Ember.A()
,
  Ember.Object.create
    id: 2
    desc: "Prechamber, ground"
    points: Ember.A()
,
  Ember.Object.create
    id: 3
    desc: "Chamber, head"
    points: Ember.A()
,
  Ember.Object.create
    id: 4
    desc: "Chamber, groin"
    points: Ember.A()
,
  Ember.Object.create
    id: 5
    desc: "Chamber, ground"
    points: Ember.A()
,
  Ember.Object.create
    id: 6
    desc: "Chamber, ground"
    points: Ember.A()
,
  Ember.Object.create
    id: 7
    desc: "Unused"
    points: Ember.A()
])

ref_points = Ember.A()

points = [
  r: 110.0
  adc: 3493
,
  r: 99.4
  adc: 3169
,
  r: 59.5
  adc: 1887
]

for point in points
  ref_points.pushObject(Ember.Object.create(point))

for sensor_no in [0...7]
  for point in points
    sensors[sensor_no].points.pushObject(Ember.Object.create({adc: point.adc}))

App.ApplicationRoute = Ember.Route.extend
	model: -> { sensors, ref_points }

App.SensorsSensorRoute = Ember.Route.extend
	model: (params) ->
    {sensor: sensors[params.sensor_no-1], ref_points}
  
  setupController: (controller, model) ->
    controller.set 'model', model
    
Utils =
  pt100: (r) ->
    table = [18.49, 20.65, 22.80, 24.94, 27.08, 29.20, 31.32, 33.43, 35.53, 37.63, 39.71, 41.79, 43.87, 45.94, 48.00, 50.06, 52.11, 54.15, 56.19, 58.22, 60.25, 62.28, 64.30, 66.31, 68.33, 70.33, 72.33, 74.33, 76.33, 78.32, 80.31, 82.29, 84.27, 86.25, 88.22, 90.19, 92.16, 94.12, 96.09, 98.04, 100.0, 101.95, 103.9, 105.85, 107.79, 109.73, 111.67, 113.61, 115.54, 117.47, 119.4]
    
    t = -200
    
    if r > table[0]

      left = 800
      i = 0
      dt = 0

      while t < 250
        break if left < 0
        
        i += 1
        
        dt = 5
        dt = 50  if (t > 110)
        dt = 40  if (t == 110)
        
        t += dt
        
        if (r < table[i])
          return t + (r - table[i]) * dt / (table[i+1] - table[i])
        
        #console.log left, t, i
        left -= 1
      
    return t
    
plot = null
    
plotFlot = (sensor_no, points, ref_points) ->
  #~ console.log points
  data = _.map(points, (point, i) -> [parseInt(point.adc), Utils.pt100(ref_points[i].r)])

  linear_regression = ss.linear_regression().data(data)
  line = linear_regression.line()
  regression_data = [[0, line(0)], [4095, line(4095)]]
  equation = {m: linear_regression.m(), b: linear_regression.b()}

  if plot
    plot.setData([
      label: "&nbsp;Given points"
      data: data
      lines: {show: false}, points: {show: true}
    ,
      label: "&nbsp;Estimation"
      data: regression_data
      lines: {show: true}, points: {show: false}
    ])
    plot.setupGrid()
    plot.draw()
    
    $("#equation").html "<p data-no=\"#{sensor_no}\" data-slope=\"#{(equation.m * 1000000).toFixed(0)}\" data-addition=\"#{equation.b.toFixed(0)}\" class=\"lead\"><var>t</var> = #{equation.m.toFixed(6)} <var>r<sub>adc</sub></var> #{equation.b.toFixed(1).replace('-', '- ')} [℃]</p>"
          
refreshDegrees = () ->
  for point in ref_points
    point.set('deg', Utils.pt100(point.r).toFixed(1))

#~ App.ApplicationController = Ember.ObjectController.extend
  #
      
App.SensorsSensorController = Ember.ObjectController.extend
  needs: "application"
  
  actions:
    save: ->
      slope = $("#equation > p").attr('data-slope')
      addition = $("#equation > p").attr('data-addition')
      sensor_no = $("#equation > p").attr('data-no')
      
      $.get "/settings/t#{sensor_no}_slope/#{slope}", ->
        #
        
      $.get "/settings/t#{sensor_no}_addition/#{-addition}", ->
        #
        
App.SensorsSensorView = Ember.View.extend
  modelObserver: ( ->
    sensor = @get('controller.model').sensor
    ref_points = @get('controller.model').ref_points
    plotFlot(sensor.id, sensor.points, ref_points)
    refreshDegrees()
  ).observes('controller.model')
    
 	didInsertElement: ->
    this._super()
    sensor_no = @get('controller.model').sensor.id    
    
    plot = $.plot("#plot", [],
      grid: {borderWidth: 1},
      legend: {position: "nw"},
      xaxis: {min: 0, max: 4100},
      yaxis: {min: -200, max: 50}
    )
    
    refreshDegrees()
    plotFlot(sensor_no, @get('controller.model').sensor.points, @get('controller.model').ref_points)

App.PtPlotComponent = Ember.Component.extend
  pointsHandler: ( ->
    plotFlot(@get('sensor_no'), @get('points'), @get('ref_points'))
  ).observes('points.@each.adc')

  refPointsHandler: ( ->
    plotFlot(@get('sensor_no'), @get('points'), @get('ref_points'))
  ).observes('ref_points.@each.r')

App.FlotKnotComponent = Ember.TextField.extend
  min: 0
  max: 4096
  step: 1
  
  changeItIntoKnot: ( ->
    this.$().knob
      min: @get('min')
      max: @get('max')
      step: @get('step')
      width: 34
      height: 34
      angleOffset: 225
      angleArc: 270
      fgColor: '#edc240'
      #~ rotation: 'anticlockwise'
      displayInput: false
      change: (v) ->
        @i.value = v
        $(@i).trigger 'change'
        refreshDegrees()
  ).on('didInsertElement')
  
  valueHandler: ( ->
    this.$().val(@get('value')).trigger('change')
  ).observes('value')

App.FlotParamComponent = Ember.TextField.extend
  changeHandler: ( ->
    refreshDegrees()
  ).on('change')

read_back = {raw: [[], [], [], [], [], [], []], translated: [[], [], [], [], [], [], []]}

updateReadings = ->
  $.get "/adc", (readings_json) ->
    readings = JSON.parse readings_json
    for sensor_no in [0...7]
      if readings.raw[sensor_no] == App.Options.broken_sensor_reading
        read_back.raw[sensor_no] = []
        read_back.translated[sensor_no] = []
        sensors[sensor_no].set 'raw_reading', '-'
        sensors[sensor_no].set 'reading', ''
        sensors[sensor_no].set 'down', true
      else
        read_back.raw[sensor_no].push(readings.raw[sensor_no])
        read_back.translated[sensor_no].push(readings.translated[sensor_no])
        
        read_back.raw[sensor_no].shift()  if read_back.raw[sensor_no].length > App.Options.sample_count
        read_back.translated[sensor_no].shift()  if read_back.translated[sensor_no].length > App.Options.samle_count
        
        average = ss.mean(read_back.translated[sensor_no])
        raw_average = ss.mean(read_back.raw[sensor_no])
        
        stdev = ss.standard_deviation(read_back.translated[sensor_no])
        raw_stdev = ss.standard_deviation(read_back.raw[sensor_no])
        
        sensors[sensor_no].set 'average', average.toFixed(0)
        sensors[sensor_no].set 'stdev', stdev.toFixed(1)
        
        sensors[sensor_no].set 'raw_average', raw_average.toFixed(0)
        sensors[sensor_no].set 'raw_accuracy', (raw_stdev * 100 / App.Options.max_sensor_reading).toFixed(1)
        
        sensors[sensor_no].set 'down', false

setInterval updateReadings, App.Options.sampling_interval_ms
