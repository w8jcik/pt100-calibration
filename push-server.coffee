exec = require('child_process').exec
express = require 'express'
os = require 'os'
colors = require 'colors/safe'

options = require './options.coffee'

app = express()
#app.use express.bodyParser()
#app.use express.methodOverride()
#app.use app.router
app.use express.static(__dirname + "/public")

options =
  port: process.env.PORT
  target_hosts: ["loader"]
  controller_host: "192.168.3.3"

runChromium = ->
  cmd = 'env DISPLAY=:0 chromium --no-first-run --incognito --kiosk http://127.0.0.1:' + options.port
  
  unless os.hostname() in options.target_hosts
    console.log 'Calling... ' + cmd
    
  if os.hostname() in options.target_hosts
    exec cmd, (error, stdout, stderr) ->
      if stdout
        unless os.hostname() in options.target_hosts
          console.log 'stdout ' + stdout
      if stderr
        console.error 'stderr ' + stderr
      if error
        return console.error('exec error: ' + error)
  else
    console.log 'Command surpressed (' + cmd + ')'
  return

push = (req, res) ->
  url = "http://#{options.controller_host}#{req.originalUrl}"

  cmd = "curl #{url}"
  
  unless os.hostname() in options.target_hosts
    console.log "Calling... #{cmd}"
  
  exec cmd, (error, stdout, stderr) ->
    if error then res.sendStatus 404 else res.send stdout

app.all "/adc", push
app.all "/settings/:setting_label/:value", push

app.listen options.port, ->
  unless os.hostname() in options.target_hosts
    console.log "Express server listening on port %d in %s mode", options.port, app.settings.env
  console.log colors.green("Gdy skończysz, wciśnij"), colors.bold.green("Ctrl+C")
  runChromium()
